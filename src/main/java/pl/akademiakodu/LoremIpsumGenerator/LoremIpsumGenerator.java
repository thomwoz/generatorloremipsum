package pl.akademiakodu.LoremIpsumGenerator;

public class LoremIpsumGenerator {
    private int id;
    private int genType;

    LoremIpsumGen loremIpsumGen;

    public int getGenType() {
        return genType;
    }

    public void setGenType(int genType) {
        this.genType = genType;
    }

    public LoremIpsumGen getLoremIpsumGen() {
        return loremIpsumGen;
    }

    public void setLoremIpsumGen(LoremIpsumGen loremIpsumGen) {
        this.loremIpsumGen = loremIpsumGen;
    }

    public String getLorem() {
        if(genType==2 )setLoremIpsumGen(new SecondIpsumGenerator());
        else setLoremIpsumGen(new FirstIpsumGenerator());
        return loremIpsumGen.generateLoremIpsum(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
