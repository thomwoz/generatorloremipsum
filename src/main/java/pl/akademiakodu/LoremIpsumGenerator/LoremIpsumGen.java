package pl.akademiakodu.LoremIpsumGenerator;

public interface LoremIpsumGen {
    String generateLoremIpsum(int i);
}
