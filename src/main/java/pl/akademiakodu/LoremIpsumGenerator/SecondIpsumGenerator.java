package pl.akademiakodu.LoremIpsumGenerator;

public class SecondIpsumGenerator implements LoremIpsumGen{
    private String[] arrayOfLorems = new String[5];

    public String[] getArrayOfLorems() {
        return arrayOfLorems;
    }


    public SecondIpsumGenerator() {
        setArrayOfLorems();
    }

    @Override
    public String generateLoremIpsum(int i) {
        StringBuilder sb = new StringBuilder();
        while (i-- != 0) {
            sb.append(arrayOfLorems[4-i%5] + "\n");
            sb.append(System.getProperty("line.separator"));
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    public void setArrayOfLorems() {
        this.arrayOfLorems[0] = "Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.";

        this.arrayOfLorems[1]="Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit. \n";

        this.arrayOfLorems[2]="You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.";

        this.arrayOfLorems[3]="Normally, both your asses would be dead as fucking fried chicken, but you happen to pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna help you. But I can't give you this case, it don't belong to me. Besides, I've already been through too much shit this morning over this case to hand it over to your dumb ass.\n";

        this.arrayOfLorems[4]="Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.";
    }
}
