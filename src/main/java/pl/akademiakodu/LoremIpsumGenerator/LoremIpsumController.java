package pl.akademiakodu.LoremIpsumGenerator;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoremIpsumController {

    @GetMapping("/loremipsumgen")
    public String greetingForm(Model model) {
        model.addAttribute("lorem", new LoremIpsumGenerator());
        return "form";
    }

    @PostMapping("/loremipsumgen")
    public String greetingSubmit(@ModelAttribute LoremIpsumGenerator loremIpsumGenerator) {
        return "result";
    }
}
